

from __future__ import print_function
from __future__ import division

import os
import matplotlib.pyplot as plt
import datetime as dt
import pandas as pd
import numpy as np
import argparse
import datetime
import pickle
import time

import torch
import torch.nn as nn
import torch.optim as optimizers
import torch.optim.lr_scheduler as lr_scheduler
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

from matplotlib import style
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer, StandardScaler
from collections import deque
from tensorboardX import SummaryWriter

style.use('ggplot')

##################### Global NN  Parameters
# ep = 5000
# bs = 256
# lr = 0.01
##########################################i


# NAME = "regression_ep={}_lr={}_bs={}_{}".format(ep, lr, bs, int(time.time()))

# writer = SummaryWriter(log_dir='logs/{}'.format(NAME))

ip_size = 8  # LSTM input features (dimensions)


def preprocess_df(df):
    df = df.drop('bw', 1)
    # df = df.drop('dly', 1)
    df = df.drop('pwr', 1)
    df.fillna(0, inplace=True)
    scaler = StandardScaler()
    # scaler = MinMaxScaler()
    # scaler = QuantileTransformer(random_state = 0)
    df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)
    df.dropna(inplace=True)
    df.fillna(0, inplace=True)
    df_y = df['dly']
    df_x = df.drop('dly', 1)
    #	df_y = df['bw']
    #	df_x = df.drop('bw', 1)
    return np.array(df_x), np.array(df_y)


class NN(nn.Module):
    """
        Our class defines MLP and LSTM models to test our data learning
    """

    def __init__(self, input_dims):
        super(NN, self).__init__()
        self.input_dims = input_dims
        # define MLP

        class MLP(nn.Module):
            def __init__(self, size):
                super(MLP, self).__init__()
                self.size = size
                self.fc_1 = nn.Linear(input_dims, self.size)
                self.fc_2 = nn.Linear(self.size, self.size)
                self.fc_3 = nn.Linear(self.size, self.size)
                self.fc_4 = nn.Linear(self.size, 1)
                self.activation = nn.Tanh()
                self.bn_1 = nn.BatchNorm1d(1)
                self.bn_2 = nn.BatchNorm1d(1)
                self.bn_3 = nn.BatchNorm1d(1)

            def forward(self, x):
                x = self.activation(self.bn_1(self.fc_1(x)))
                x = self.activation(self.bn_2(self.fc_2(x)))
                x = self.activation(self.bn_3(self.fc_3(x)))
                x = self.fc_4(x)
                return x.view(-1, 1)

        self.mlp = MLP(size=8)

        class LSTM(nn.Module):
            """
                This is our lstm class
            """

            def __init__(self, input_size, seq_length, hidden_size, num_layers):
                super(LSTM, self).__init__()
                self.num_layers = num_layers
                self.sequence_len = seq_length
                self.hidden_size = hidden_size
                self.lstm = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True, dropout=0)
                self.fc_1 = nn.Linear(hidden_size, 32)
                self.fc_2 = nn.Linear(32, 1)

            def forward(self, input):
                h0 = torch.randn(self.num_layers, input.size()[0], self.hidden_size,
                                 requires_grad=True)  # torch.zeros (num_of_lstm_layers, batch_size, input_size)
                c0 = torch.randn(self.num_layers, input.size()[0], self.hidden_size, requires_grad=True)  # torch.zeros
                output, hidden = self.lstm(input, (h0, c0))
                output = self.fc_2(F.relu(self.fc_1(output))).view(-1, 1)
                return output

        self.my_lstm = LSTM(input_size=ip_size, seq_length=1, hidden_size=64, num_layers=3)

    def train_net(self, model, train_data, train_labels, test_data, test_labels, args):

        # before proceeding, let's build our dataloader
        class dataset(Dataset):
            def __init__(self, example_array, label_array):
                super(dataset, self).__init__()
                self.examples = example_array
                self.labels = label_array

            def __len__(self):
                return self.examples.shape[0]

            def __getitem__(self, item):
                return (torch.Tensor(self.examples[item]), torch.Tensor(self.labels[item]))

        train_dataset = dataset(example_array=train_data, label_array=train_labels)
        test_dataset = dataset(example_array=test_data, label_array=test_labels)

        train_loader = DataLoader(dataset=train_dataset, batch_size=args.bs, shuffle=True, num_workers=4)
        test_loader = DataLoader(dataset=test_dataset, batch_size=args.bs, shuffle=False, num_workers=4)
        optimizer = optimizers.RMSprop(model.parameters(), lr=args.lr)
        criterion = nn.MSELoss()
        lr_final = 5e-5
        LR_decay = (lr_final / args.lr) ** (1. / args.epochs)
        scheduler = lr_scheduler.ExponentialLR(optimizer=optimizer, gamma=LR_decay)
        if not os.path.exists(args.models_path):
            print('log: Making models directory {}'.format(args.models_path))
            os.mkdir(args.models_path)
        if args.model:
            saved_model_path = os.path.join(args.models_path, 'model-{}.pt'.format(args.model))
            if os.path.exists(saved_model_path):
                print('log: Reloading saved model {}'.format(saved_model_path))
                model.load_state_dict(torch.load(saved_model_path), strict=False) # this allows you to change layers

        NAME = "regression_ep={}_lr={}_bs={}_{}".format(args.epochs, args.lr, args.bs, int(time.time()))
        writer = SummaryWriter(log_dir='logs/{}'.format(NAME))

        # check cuda availability
        if args.cuda:
            model.cuda(device=args.device)
        for e in range(1, args.epochs + 1):
            epoch_loss = 0.0
            epoch_acc = 0.0
            model.train()  # set in train mode
            for i, data in enumerate(train_loader):
                batch_data_tensor, batch_labels_tensor = data
                batch_data_tensor = batch_data_tensor.cuda(device=args.device) if args.cuda else batch_data_tensor
                batch_labels_tensor = batch_labels_tensor.cuda(device=args.device) if args.cuda else batch_labels_tensor
                output = model(batch_data_tensor)
                # define 1% accuracy metric
                epoch_acc += (torch.abs(batch_labels_tensor-output)/(batch_labels_tensor)<0.01).sum().item()
                loss = torch.sqrt(criterion(output, batch_labels_tensor))
                writer.add_scalar("loss", loss, args.epochs)
                epoch_loss += loss.item()
                writer.add_scalar("avg_loss", epoch_loss, args.epochs)
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()
                model.zero_grad()
            # need to scale the learning rate after each epoch
            scheduler.step()
            # if e % 1 == 0:
            print('epoch ({}/{}), batch_loss = {}, batch_acc = {}%, (lr: {})'.format(e,
                                                                                     args.epochs,
                                                                                     loss.item(),
                                                                                     epoch_acc*100.0/(len(train_loader)*args.bs),
                                                                                     optimizer.param_groups[0]['lr']))
            if e % 10 == 0:
                save_model_path = os.path.join(args.models_path, 'model-{}.pt'.format(e+args.model))
                print('log: -----------------> Saving model {}'.format(save_model_path))
                torch.save(model.state_dict(), save_model_path)
                # save only last 5 models
                remove_model_path = os.path.join(args.models_path, 'model-{}.pt'.format(e+args.model-500))
                if os.path.exists(remove_model_path):
                    print('log: -----------------> Removing model {}'.format(remove_model_path))
                    os.remove(remove_model_path)

                # test the model now
                _, _ = self.test_model(args=args, model=model, criterion=criterion, train_loader=train_loader,
                                       test_loader=test_loader, writer=writer, step=e)
                #
                # prdict = predictions.data.numpy()
                # train_predict = predictions_train.data.numpy()
                #
                # rms_value = np.sqrt(np.mean(np.power((test_labels - prdict), 2)))
                # print("----> Test Root Mean Square Error = {:.7f}".format(rms_value))
                #
                # plt.subplot(121)
                # plt.title('validation')
                # plt.plot(range(len(test_labels)), test_labels, color='red', label="validation labels")
                # plt.plot(range(len(prdict)), prdict, color='blue', label="validation predictions")
                # plt.xlabel('no of test values')
                # plt.ylabel('normalized delay value')
                # plt.legend(loc='upper left') #bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
                # # plt.savefig('op_pred/multibank_regression_ep%d_lr%f_bs%d_%s.png'%(ep, lr, bs, time))
                #
                # # plt.figure()
                # plt.subplot(122)
                # plt.title('training')
                # plt.plot(range(len(train_data)), train_labels, color='red', label="train labels")
                # plt.plot(range(len(train_predict)), train_predict, color='blue', label="train predictions")
                # plt.xlabel('no of train values')
                # plt.ylabel('normalized delay value')
                # plt.legend(loc='upper left') #bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
                # mng = plt.get_current_fig_manager()
                # mng.resize(*mng.window.maxsize())
                # plt.show()

        print('\n testing now... \n')
        predictions, predictions_train = self.test_model(args=args, model=model, criterion=criterion,
                                                         train_loader=train_loader, test_loader=test_loader,
                                                         writer=writer, step=e)
        writer.close()
        return (predictions, predictions_train)

    @torch.no_grad()
    def test_model(self, args, model, criterion, train_loader, test_loader, writer, step):
        model.eval()  # set in eval mode
        output_test, output_train = None, None
        test_loss, train_loss = 0., 0.
        for i, data in enumerate(train_loader):
            batch_data_tensor, batch_labels_tensor = data
            batch_data_tensor = batch_data_tensor.cuda(device=args.device) if args.cuda else batch_data_tensor
            batch_labels_tensor = batch_labels_tensor.cuda(device=args.device) if args.cuda else batch_labels_tensor
            batch_output_train = model(batch_data_tensor)
            output_train = batch_output_train if output_train is None else torch.cat((output_train, batch_output_train),
                                                                                     dim=0)
            train_loss += torch.sqrt(criterion(batch_output_train, batch_labels_tensor))
        data = None

        rms_value = 0.0
        epoch_acc = 0.0
        print('testing on approx. {} examples...'.format(len(test_loader)*args.bs))
        for i, data in enumerate(test_loader):
            batch_data_tensor, batch_labels_tensor = data
            batch_data_tensor = batch_data_tensor.cuda(device=args.device) if args.cuda else batch_data_tensor
            batch_labels_tensor = batch_labels_tensor.cuda(device=args.device) if args.cuda else batch_labels_tensor
            batch_output_test = model(batch_data_tensor)
            output_test = batch_output_test if output_test is None else torch.cat((output_test, batch_output_test),
                                                                                  dim=0)
            test_loss += torch.sqrt(criterion(batch_output_test, batch_labels_tensor))
            rms_value += torch.sqrt(torch.mean(torch.pow((batch_labels_tensor - batch_output_test), 2)))
            epoch_acc += (torch.abs(batch_labels_tensor - batch_output_test) / (batch_labels_tensor) < 0.01).sum().item()

        print("Log: Test Root Mean Square Error = {:.7f}, Test 1% accuracy = {}%".format(rms_value,
                                                                                         epoch_acc*100.0/(len(test_loader)*args.bs)))
        writer.add_scalar("test loss", test_loss, step)
        return output_test, output_train


def diff_addr_f(current, future):
    return (float(future) - float(current))


def diff_addr_p(prev, current):
    return (float(current) - float(prev))


def trainer(args):

    # save_data_path is a path to a folder to save our dataset numpy arrays in
    if not os.path.exists(args.save_data_path):
        print('Log: No saved data found, generating data now...')

        df = pd.read_csv('/home/annus/Desktop/dataset_trainingsdata_multiple_banks_bw_power.csv',
                         names=["time", "addr", "dly", "bw", "pwr"])

        df['addr_f'] = df['addr'].shift(-1)  # future value of delay
        df['addr_p'] = df['addr'].shift(1)

        df['diff_f1'] = list(map(diff_addr_f, df['addr'], df['addr_f']))
        df['diff_p1'] = list(map(diff_addr_p, df['addr_p'], df['addr']))

        df['diff_f2'] = list(map(diff_addr_f, df['addr'], (df['addr'].shift(-2))))
        df['diff_p2'] = list(map(diff_addr_p, (df['addr'].shift(2)), df['addr']))

        df.fillna(0, inplace=True)

        for n in range(1, 5):
            plt.subplot(2, 2, n)
            if n == 1:
                plt.title('address vs. time')
                plt.plot(df['time'], df['addr'])
            elif n == 2:
                plt.title('bw vs. time')
                plt.plot(df['time'], df['bw'])
            elif n == 3:
                plt.title('pwr vs. time')
                plt.plot(df['time'], df['pwr'])
            elif n == 4:
                plt.title('dly vs. time')
                plt.plot(df['time'], df['dly'])
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()

        print(df.head())
        times = sorted(df.index.values)
        last_30pct = times[int(0.90 * len(times))]
        validation_df = df[df.index >= last_30pct]
        train_df = df[(df.index < last_30pct)]

        train_x, train_y = preprocess_df(train_df)
        validate_x, validate_y = preprocess_df(validation_df)

        train_x = train_x.reshape((-1, 1, ip_size))
        validate_x = validate_x.reshape(-1, 1, ip_size)

        train_y = np.reshape(train_y, newshape=(len(train_y), 1))
        validate_y = np.reshape(validate_y, newshape=(len(validate_y), 1))

        os.mkdir(args.save_data_path)
        print('Log: Made directory {}. Saving data now...'.format(args.save_data_path))
        np.save(os.path.join(args.save_data_path, 'train_x.npy'), train_x)
        np.save(os.path.join(args.save_data_path, 'train_y.npy'), train_y)
        np.save(os.path.join(args.save_data_path, 'validate_x.npy'), validate_x)
        np.save(os.path.join(args.save_data_path, 'validate_y.npy'), validate_y)
        # now release the occupied memory and load with memory mapping
        train_x = None
        train_y = None
        validate_x = None
        validate_y = None
        df = None
        validation_df = None
        train_x = np.load(os.path.join(args.save_data_path, 'train_x.npy'), mmap_mode='r')
        train_y = np.load(os.path.join(args.save_data_path, 'train_y.npy'), mmap_mode='r')
        validate_x = np.load(os.path.join(args.save_data_path, 'validate_x.npy'), mmap_mode='r')
        validate_y = np.load(os.path.join(args.save_data_path, 'validate_y.npy'), mmap_mode='r')

    else:
        print('Log: Saved dataset found! Loading now...')
        train_x = np.load(os.path.join(args.save_data_path, 'train_x.npy'), mmap_mode='r')
        train_y = np.load(os.path.join(args.save_data_path, 'train_y.npy'), mmap_mode='r')
        validate_x = np.load(os.path.join(args.save_data_path, 'validate_x.npy'), mmap_mode='r')
        validate_y = np.load(os.path.join(args.save_data_path, 'validate_y.npy'), mmap_mode='r')

    print('size:train_data, train_labels: ', train_x.shape, train_y.shape)
    print('size: validate_ data, validate_labels: ', validate_x.shape, validate_y.shape)

    net = NN(input_dims=ip_size)

    ###############################################################################################3
    # pass 0 for model if you are starting from scratch
    predictions, predictions_train = net.train_net(model=net.my_lstm, train_data=train_x, train_labels=train_y,
                                                   test_data=validate_x, test_labels=validate_y, args=args)

    if not args.cuda:
        prdict = predictions.data.numpy()
        train_predict = predictions_train.data.numpy()
    else:
        prdict = predictions.detach().cpu().numpy()
        train_predict = predictions_train.detach().cpu().numpy()

    rms_value = np.sqrt(np.mean(np.power((validate_y - prdict), 2)))
    print("Root Mean Square Error = {:.7f}".format(rms_value))

    time = datetime.datetime.now()

    plt.plot(range(len(validate_y)), validate_y, color='red', label="validation values")
    plt.plot(range(len(prdict)), prdict, color='blue', label="predicted values")
    plt.xlabel('no of test values')
    plt.ylabel('normalized delay value')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    # plt.savefig('op_pred/multibank_regression_ep%d_lr%f_bs%d_%s.png'%(ep, lr, bs, time))

    plt.figure()
    plt.plot(range(len(train_y)), train_y, color='red', label="validation values")
    plt.plot(range(len(train_predict)), train_predict, color='blue', label="predicted values")
    plt.xlabel('no of train values')
    plt.ylabel('normalized delay value')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', dest='save_data_path', type=str)
    parser.add_argument('--bs', dest='bs', type=int)
    parser.add_argument('--epochs', dest='epochs', type=int)
    parser.add_argument('--lr', dest='lr', type=float)
    parser.add_argument('--models_path', dest='models_path', type=str)
    parser.add_argument('--model', dest='model', type=int, default=0)
    parser.add_argument('--cuda', dest='cuda', type=int, default=None)  # this is for the gpu
    parser.add_argument('--device', dest='device', type=int, default=None)  # this is for the gpu
    args = parser.parse_args()
    trainer(args)
























